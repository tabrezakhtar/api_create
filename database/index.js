const customers = require('../deli_customers.json');

function getCustomers() {
  return customers.map(c => ({first_name: c.first_name, last_name: c.last_name, email: c.email}));
}

module.exports = {getCustomers}