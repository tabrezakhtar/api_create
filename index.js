const express = require('express')
const cron = require('node-cron');
const routes = require('./routes');
const { processOrders } = require('./orderProcessor');

const app = express();
const port = process.env.PORT || 8080;

app.use('/api', routes);

app.listen(port, () => {
  console.log(`App started on port: ${port}`)
});

cron.schedule('1 * * * * *', () => {
  processOrders();
});