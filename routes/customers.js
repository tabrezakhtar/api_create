const database = require('../database');

async function getCustomers(req, res) {
  try {
    const customers = database.getCustomers();
    res.status(200).send(customers);
  } catch (err) {
    res.status(500).json({ message: err })
  }
}

module.exports = {getCustomers};