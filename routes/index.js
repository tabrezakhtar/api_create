const express = require('express');
const router = express.Router();
const customerRoutes = require('./customers');

router.get('/customers', customerRoutes.getCustomers);

module.exports = router;